#! /usr/bin/env racket
#lang racket
;; call from inside class directory:
;;   $ cd  ../management/questionnaires/EC1-2022-Semester1/
;; ; $ ../../../scripts/class-questionnaire-result-plotting.rkt

;; in data directory have frequencies.txt and text-for-questions-and-responses.txt
;; ; will output png plots into imgs directory

;; ; re-writing script from:
;; ;;  gl/class-questionnaire-result-plotting/plot-questionnaire-results.rkt

(define class-name
  (if (< 0 (vector-length (current-command-line-arguments)))
      (vector-ref (current-command-line-arguments) 0)
      "My-Class"))

(define imgs-dir "imgs")
(unless (directory-exists? imgs-dir) (make-directory imgs-dir))
 
(require csv-reading pict plot racket/draw
         (only-in slideshow para)
         (only-in slideshow/text with-size)
         (only-in pict-abbrevs save-pict))
(define img-ext 'png) ;; for send plot save-file f-name
;; svg need draw and dc, sort of a pain, save for later...
;;;;;;;;; settings for question paragraph
(define img-bgd-clr "white")
(define qtxt-size 20) ;; 16 ; smaller than resps, 18 ; ok?

;;;;;;;;; settings for plots;
(define p-width 800)
(define height-per-qstn 50)
(define p-label-txt-size 15)
(define p-label-num-size 14)
(define line-width 10)

;; get data
(define questionnaire-text (file->string "data/questionnaire-text.txt"))
(define questionnaire-sections (string-split questionnaire-text #px"\n\n+"))
(define respondents-text (first questionnaire-sections))
;; respondents-text ; "対象人,\n回答人," ;  ok!
(define question-sections (rest questionnaire-sections))
;;(first question-sections) ; ok!
(define frequency-list (csv->list (open-input-file "data/frequencies.txt")))
(define respondents-data (take frequency-list 2))
; respondents-data ; '(("75") ("53")) ;  ok!
(define respondents-list (map cons
                              (map car (csv->list respondents-text))
                              respondents-data))
;; respondents-list ; '(("対象人" "75") ("回答人" "53")) ; ok!
(define freqs (drop frequency-list 2)) ;; freqs ; ok!

;; adjust Japanese text so slideshow's para procedure can do linebreaks
(define (ja-str-for-line-breaks str)
  ;; put a space before different opening brackets
  (define spaces-before
    (regexp-replace* #rx"([「（『])" str " \\1"))
  ;; put a space after different closing brackets
  (define spaces-after (regexp-replace* #rx"([」)）,。、』?:：])"
					spaces-before
					"\\1 "))
    spaces-after)

;; run text through ja-string-cleaner above first
(define (qtext->qpict txt (fnt-sze qtxt-size))
  (define q-p
    (with-size fnt-sze
      (para #:width p-width txt)))
  (define-values (w h)
    (values (pict-width q-p)(pict-height q-p)))
  (lt-superimpose (filled-rectangle w h
                                    #:color img-bgd-clr
                                    #:draw-border? #f)
                  q-p))

;; get question-sections (with responses) from text-for-questions-n-responses.txt
(define (qstn-sctn->q-n-rsps txt) ;; if ja encoding ja-str-spaces
  (define lines (string-split txt "\n"))
  (define qstn-line (car (csv->list (string-trim (first lines)))))
  (define response-lines (map car (map
                                   (lambda (l)
                                     (csv->list (string-trim l)))
                                   (rest lines))))
  (values qstn-line response-lines))
;; (qstn-sctn->q-n-rsps (first question-sections)) ; ok! 
;; '("1" "授業時間外の学習...1週間の平均的な時間を回答してください。")
;; '(("6" "10時間以上") ("5" "5時間以上10時間未満") ("4" "3時間以上5時間未満") ("3" "1時間以上3時間未満") ("2" "1時間以下") ("1" "全くしていない"))

;; use a section and its frequencies
(define (plt-to-pict-n-file sctn frqs) ;; question.png
  (define-values (qsn-lst rsps-list)
    (qstn-sctn->q-n-rsps sctn))
  (define rsps-lnth (length rsps-list))
  (define p-height (* height-per-qstn rsps-lnth))
  (define f-name (path-add-extension
                  (build-path imgs-dir (first qsn-lst))
                  (string-append "." (symbol->string img-ext))))
  (define-values (regsd-num regsd-str respd-num respd-str)
    (values (string->number (second (first respondents-list)))
	    (first (first respondents-list))
	    (string->number (second (second respondents-list)))
	    (first (second respondents-list))))
  (define x-max-add (* .02 regsd-num))
  (define x-max (+ regsd-num x-max-add))

;; ?? Making question pict separate and superimposing later?
  (define qsn-pict (qtext->qpict ;; use string-join with eng-text
                    (string-append (first qsn-lst)
                                 ". "
                                 ;; later 'ja opt/condition
                                 (ja-str-for-line-breaks
                                  (second qsn-lst))
                                 )))

  (define (regi-responded-lines-end-label x y num str )
    (list
     (point-label (vector x y) str
                  #:anchor 'top-right #:color "black" #:point-size 0 #:size p-label-txt-size)
     (point-label (vector x y) num ;; 'bottom-right
                  #:anchor 'right #:point-size 0 #:size p-label-num-size)))

  (define (regi-responded-line y num str (clr "light gray") (wdth line-width))
    (list (lines (list (vector 0 y)(vector num y)) #:color clr #:width wdth)
          (regi-responded-lines-end-label num y
                                          (number->string num)
                                          str)))
  ;;; don't need label question because will vl-append a slideshow para...
  (define (reponse-frequency-line result-num y response-string)
    (list
     (lines (list (vector 0 y)(vector result-num y))
            #:color "Cornflower Blue" #:width line-width)
     (point-label (vector result-num y)
                  (string-append
                   (number->string result-num) " "
                   response-string)
                  #:anchor 'left #:point-size 0 #:size p-label-num-size)))

  (define (response-lines rst-lst rsps-list)
  ;; Question-List, Result-List Response-List
    (define resp-txt-list (map second rsps-list))
    (define rsps-lnth (length rsps-list))
    (define rslts-freq-list (map string->number ;; frequencies
                         (rest rst-lst)))
    (define ys (range 3 (+ 3 rsps-lnth)))
    (map reponse-frequency-line rslts-freq-list ys resp-txt-list))

  (define the-plot-parts
           (list
        (point-label (vector 0 0) class-name
                     #:anchor 'bottom-left #:color "gray" #:point-size 0)
        (vrule 0 0 (+ 3 rsps-lnth))
        (vrule (+ x-max-add regsd-num) 0 (+ 3 rsps-lnth))
        (hrule 0)
        ;; show registered line
	(regi-responded-line 1 regsd-num regsd-str)
        ;; show reponded line
	(regi-responded-line 2 respd-num respd-str "dark gray")
	;; show frequencies line and Question text
        (response-lines frqs rsps-list)))
  ;;;; all the plot parts above

  ;;;; ; actual plotting below
  ;;;; ; ; for returning picts
  (define plt-pict
    (parameterize
        ((plot-decorations? #f)
         (plot-width p-width)
         (plot-height p-height)
         )
      (plot-pict  the-plot-parts
                  #:x-min 0 #:x-max x-max 
                  #:y-min 0 #:y-max (+ 3 rsps-lnth))))
  (define qpara-w-plot (vl-append qsn-pict plt-pict))
  ;; write out only plt-picts to imgs/plot-part, then generate sxml later
  (save-pict f-name qpara-w-plot img-ext)
  ;;  qpara-w-plot ;; for previewing in DrRacket, uncomment this line and
  ;; ; uncomment the match-define code below to see plots as q1 q2 etc. in DrR
)
;;  Uncomment this match-define along with qpara-w-plot for previews
#;(match-define (list q1 q2 q3-1 q3-2 q3-3 q4 q5 q7 q8 q9 q10 q11 q12)
  (map plt-to-pict-n-file questions-list freqs)) ; ok!

;; (plt-to-pict-n-file (first question-sections) (first freqs)) ; 1.png ; ok!
(for-each plt-to-pict-n-file question-sections freqs) ;; just output pngs for now

  
