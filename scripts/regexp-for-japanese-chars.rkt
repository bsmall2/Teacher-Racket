#lang racket

(provide (all-defined-out))

(define regexp-spaces #px"　| ")
(define regexp-parens-open #px"\\(|（")
(define regexp-parens-closed #px"\\)|）")
(define regexp-parens #px"\\(|（|\\)|）")
