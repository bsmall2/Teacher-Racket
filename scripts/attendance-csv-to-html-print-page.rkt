#! /usr/bin/env racket
#lang racket
;; Excelを使うな
;; https://mstdn.jp/web/tags/OkumuraHaruhiko
;; ; https://oku.edu.mie-u.ac.jp/~okumura/SSS2013.pdf
;; ; https://oku.edu.mie-u.ac.jp/~okumura/blog/node/2287
(require csv-reading) 
(require scribble/html) ; for output-xml
(require scribble/html/html) ; for div and class: etc...

(define default-infile (make-parameter "attendance.csv"))
(define default-teacher (make-parameter "スモール、ブライアン"))
(define default-period (make-parameter "月1")) ;; get this from unipa data
  
;; (current-command-line-arguments) ; ok! '#() , '#("data.csv")

(define ccla (current-command-line-arguments))
(when (0 . < . 
         (vector-length ccla))
  (default-infile (vector-ref ccla 0)))
(when (1 . < .
         (vector-length ccla))
  (default-teacher (vector-ref ccla 1)))

(define in-file (default-infile))
(define teacher (default-teacher))
;; in-file ; ok!
;; ; ./attendance-date-to-print-out.rkt data.csv ; "data.csv"
;; ; ./attendance-date-to-print-out.rkt ; "attendance.csv"

(define in-list (csv->list (open-input-file in-file)))
;; in-list ; ok!

;; list-reference and table-data, list of lists
(define (get-head-valu-list ref lists)
  ;; header names in first row
  (define header (list-ref (first lists) ref))
  ;; the second row is class-type info for dates, blank for meta-data
  ;; the third row is dates and other column info, blankd for class-data
  ;; the fourth row has class-meta data for headers in first row
  (define headval (list-ref (fourth lists) ref))
  (list header headval))

(define class-info-lists
  (list
   (get-head-valu-list 0 in-list) ; NenDo
   (get-head-valu-list 1 in-list) ; Gakki
   (get-head-valu-list 2 in-list) ; CCode
   (get-head-valu-list 3 in-list) ; CName
   (list "教員" teacher)))
  
(define (get-useful-cols lst)
  (list* (list-ref lst 4) (list-ref lst 5)
         (drop lst 13)))

(define useful-cols
  (map get-useful-cols in-list))

(define (remove-year date-string)
  (regexp-replace #px"\\d{1,4}/" date-string ""))
(define (remove-You-Gen period-string)
  (regexp-replace #px"限"
                  (regexp-replace #px"曜" period-string "")
                  ""))
(define (remove-spaces name-string)
  (regexp-replace* #px"　| " name-string ""))
(define (remove-paren-name name-string)
  (regexp-replace #px"（.*）" name-string ""))
        
(define (shorten-name lst)
  (list* (first lst)
         (remove-paren-name
          (remove-spaces (second lst)))
         (drop lst 2)))

(define headers (map remove-year (first useful-cols)))
(define periods (map remove-You-Gen (second useful-cols)))
(define data (map shorten-name (drop useful-cols 3)))
;; (list headers periods data) ; ok!
;; headers ; ok! remove-year
;; periods ; ok! remove-曜 限

(define (list->xml-tr lst)
  (tr
   (map td lst)))

(define out-html (open-output-file "Attendance-Report.html" #:exists 'replace)) ;; won't write to file until closed

(output-xml
 (html
  (head (title "Attendance Report")
        (map style (list
                    "h1 { font-weight: 400; font-size: 14pt; }"
                    ;; decrease cell space: small font-size, less padding
                    "td { font-size: 8pt; padding: 2pt 4pt 4pt 2pt; }"
                    ;; Second table of attendance, smaller
                    ;; ; Small font-size for dates in first row
                    "table:nth-child(n+2) tr:first-child td:nth-child(n+3)
                        { font-size: 6pt;}"
                    ;; Same font-size and text-alignment for class-period header and character for attendance/tardiness etc.
                    ;; 出Attended 公Excused/Dispensation 遅Tardy/Late 早Fled/Premature 欠Skipped/abSent 
                    "table:nth-child(n+2) tr:nth-child(n+2) td:nth-child(n+3) 
                        { font-size: 6pt; text-align: center; }"
                    ;; Smaller font for Student ID number
                    "tr:nth-child(n+3) td:first-child 
                        { font-size: 6pt; } "

                    )) ; end map style
        ) ; end head
  (body
   (table
    (map list->xml-tr (list
                       (map first class-info-lists)
                       (map second class-info-lists))))
   (h1 "授業出欠席一覧")
   (table
    (map list->xml-tr (list*
                        headers
                        periods
                        data)))
   ))
 out-html)

(close-output-port out-html)
;; the rkt-curdir.. script was inside a procedure and let block
;; ; the blocks auto-magically close ports for the programmer
