# Teacher-Racket

Racket scripts and Free Software ideas for classroom teaching.


## Scripts

Two scripts for classroom management using .csv data are ready. I use the scripts to print roll-sheet pages and attendance reports from data downloaded from Universal Passport. Example data and pages are in the `EC1-2018-S1` directory within the `classes` and `management` directories. After making the scripts 'executable'[^1] `cd`(change) into the  `EC1-2018-S1` directory and:

```
$ ../../../scripts/learner-list-start-to-roll-sheet
```

Sometimes the details of making a script executable are distracting so I frequently do:

```
$ racket   ../../../scripts/learner-list-start-to-roll-sheet
``` 

## Aims, Motivations

One aim for sharing here is to help people avoid Office Software by using tools that are better for thinking and working like a decent human being. Haruhiko Okumara(奥村晴彦) posted reasons for not using Excel (Excelを使うな). While explaining the spreadsheet software's mathematical errors Okumura mentions 'chart junk'. Edward Tufte discourages the use and consumption of powerpoint slideshows. Solomon Messing explains visualizations to avoid (pie-charts) and how good visualizations and level the technical field for wider participation. 

Emacs and Racket let us find ways to work with spreadsheets and slideshows and visualizations and to partipate in the 'Great Conversation' of Neil Postman and Robert Maynard Hutchins. We can work with computers and also see into the liberal arts. Emacs-lisp and Scheme/Racket are tools, or environments, can ease us into math[^2] and logic[^3] in addition to providing wise ways to work.  



[^1]: Make a script executable in a shell or terminal: `$ chmod +x my-script.rkt`, for `emacs`'s `dired` [Alt-m] (`M-m`) and `+x` will work too.

[^2]: The history of math appears again and again while learning Lisp->Scheme->Racket. Paul Graham explains that Lisp does not get stale: it's classic like math or the quicksort algorithm. Alonzo Church's lambda calculus provided the base for Lisp and Scheme. Alonzo Church's λ(lambda) notation is a replacement for the ^(caret) notation of Alfred North Whitehead and Bertrand Russel's _Principia Mathematica_. With Alfred North Whitehead (_Science and Society_'s _Religion and Science_) and Bertrand Russel (_Mortals and Others_, _On Education_, _Conquest of Happiness_, Pugwash's Russel-Einstein Manifesto, The Russel Tribunal...) we can move from computers and math into history and society, war and peace, extinction and rebellion. 

[^3]: And Raymond Smullyan helps us with more recent writings (for all ages!) on the logic and philosophy brought to our attention by the other thinkers. Raymond Smullyan's _To Mock a Mockingbird_ is mentioned in _The Schemer's Guide_. Alonzo Church is mentioned too, just as in _How to Design Programs_ and _The Little Schemer_.

