
** English

** Toroku, Minamata, Nauru
#+begin_quote
Andre Gunder Frank, in analyzing “metropolis-satellite” relations through Latin American history as a chain of successive subjections, has highlighted the fact that the regions now most underdeveloped and poverty-stricken are those which in the past had had the closest links with the metropolis and had enjoyed periods of boom.(20) Having once been the biggest producers of goods exported to Europe, or later to the United States, and the richest sources of capital, they were abandoned by the metropolis when for this or that reason business sagged. Potosí is the outstanding example of this descent into the vacuum.
 -- pp. 31-32 

 - (20) Andre Gunder Frank, Capitalism and Underdevelopment in Latin America (New York and London: Monthly Review Press, 1967).
#+end_quote

#+begin_quote
The uncommonly arrogant Lima oligarchy continued enriching itself and amassing symbols of its power in the palaces and Carrara marble mausoleums which sprouted amid sandy deserts. Once it had been Potosí’s silver that nourished the great families of the capital city; now they lived from bird-droppings and the shiny white clots in the nitrate fields—more vulgar means to the same elegant ends. Peru thought it was independent, but Britain had taken Spain’s place. The country felt rich, according to José Carlos Mariátegui, and the state carelessly used up its credit, living prodigally and mortgaging its future to British high finance. In 1868, the state’s expenditures and debts far exceeded the value of its sales abroad. The guano deposits served as guarantee for British loans, and Europe juggled prices. The plunder of the exporters created havoc; what nature had accumulated over millennia on the islands was squandered in a few years.
 -- p. 140 
#+end_quote

** 日本語



